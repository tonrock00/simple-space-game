﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerscript : MonoBehaviour
{
    public Vector3 playerPos;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        playerPos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = playerPos;
        if (Input.GetKey(KeyCode.W))
        {
            playerPos.y += speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            playerPos.y -= speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            playerPos.x -= speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            playerPos.x += speed * Time.deltaTime;
        }
    }
}
